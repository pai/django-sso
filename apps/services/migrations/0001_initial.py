# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-09-24 06:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=32, null=True, unique=True)),
                ('url', models.CharField(max_length=255, unique=True)),
                ('picture', models.TextField(blank=True, null=True, verbose_name='picture')),
                ('cookie_domain', models.CharField(blank=True, max_length=255, null=True)),
                ('redirect_wait', models.PositiveIntegerField(default=2000)),
                ('role', models.SmallIntegerField(blank=True, choices=[(-1, 'Staff'), (0, 'Cityzen'), (1, 'Institutional'), (2, 'Professionist'), (4, 'Company'), (6, 'Istitutional Partition Item'), (7, 'App')], null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('unsubscribed_at', models.DateTimeField(blank=True, null=True)),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subscriptions', to='services.Service')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subscriptions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TOS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language', models.CharField(max_length=3)),
                ('text', models.TextField()),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='terms_of_service', to='services.Service')),
            ],
        ),
    ]
