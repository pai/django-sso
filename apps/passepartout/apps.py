from django.apps import AppConfig


class PassepartoutConfig(AppConfig):
    name = 'passepartout'
