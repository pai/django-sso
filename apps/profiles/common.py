ROLE_CHOICES = (
    (-1, 'Staff'),
    (0, 'Cityzen'),
    (1, 'Institutional'),
    (2, 'Professionist'),
    (4, 'Company'),
    (6, 'Istitutional Partition Item'),
    (7, 'App'),
)

PROFILE_FIELDS = (
    'email',
    'password',
    'role',
    'first_name', 'last_name',
    'description', 'picture', 'birthdate',
    'latitude', 'longitude',
    'country',
    'address',
    'language'
)
