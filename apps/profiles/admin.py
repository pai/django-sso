from django.contrib import admin

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# from django.contrib.auth.models import User

from .models import User as UserModel


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'last_login')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'last_login')

# Re-register UserAdmin
# admin.site.unregister(User)
admin.site.register(UserModel, UserAdmin)
