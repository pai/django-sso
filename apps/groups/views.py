from rest_framework import viewsets

from .serializers import GroupSerializer
from .models import Group


class GroupViewSet(viewsets.ReadOnlyModelViewSet):

    serializer_class = GroupSerializer
    lookup_field = 'id'

    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return Group.objects.none()
        else:
            if user.is_staff:
                return Group.objects.all()
            else:
                return Group.objects.filter(user__in=[user])
