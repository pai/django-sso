from django.conf.urls import url

from .views import GroupViewSet


urlpatterns = [
    url(r'^$', GroupViewSet.as_view({'get': 'list'}), name="group-list"),
]
