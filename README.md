# django-sso

> Django single sign-on

This is alpha software and is under heavy development.

## INSTRUCTIONS


## REST API

  - **login**

    path: */api/v1/auth/login/*

    methods: POST
    
    get params:
      - next (protected service calling url)
      
    if there is more than one sso domain instance returns a passepartout_redirect_url to redirect to in order to enable multi-domain access to caller browser device.

  - **logout**

    path: */api/v1/auth/logout/*

    methods: POST
    
    get params:
      - next (protected service calling url)
      
    if there is more than one sso domain instance returns a passepartout_redirect_url to redirect to in order to cleanup caller browser device.

  - **register a new user**:

    path: */api/v1/auth/registration/*
    
    methods: POST


  - **send password reset email**

    path: */api/v1/auth/password/reset/*:

    methods: POST


  - **retrieve logged-in user profile**

    path: */api/v1/auth/user/*
    
    methots: GET

  - **check existence per username/email and return profile id**

    path: */api/v1/profiles/users/check/*

    methods: GET
    
    params: ?username=<username> or ?email=<email>

  - **get user id:rev pairs**

    path: */api/v1/profiles/users/revisions/*

    methods: GET

    Cached for 120 seconds.

  - **create a new user (is_staff users only)**

    path: */api/v1/profiles/users/*

    methods: POST

    GET params:
        - skip_confirmation (true, false)
        - password_is_hashed (true, false)

  - **get or patch user profile (profile owner or is_staff users)**

    path: */api/v1/profiles/users/<user_id>/*

    methods: GET,PATCH
    
    GET params:
        - skip_confirmation (true, false) **staff only**

  - **unsubscribe from SSO (profile owner)**

    path: */api/v1/profiles/users/<user_id>/unsubscribe/

    methods: POST
    
    body:
        {
        'password': '***'
        }

  - **list user subscriptions (profile owner)**

    path: */api/v1/profiles/users/<user_id>/subscriptions/

    methods: GET
    

  - **get user subsctiption details (profile owner)**

    path: */api/v1/profiles/users/<user_id>/subscriptions/<subscription_id>/

    methods: GET
    

  - **unsubscribe from service (profile owner)**

    path: */api/v1/profiles/users/<user_id>/subscriptions/<subscription_id>/unsubscribe/

    methods: POST
    
    body:
        {
        'password': '***'
        }

  - **list available services**

    path: */api/v1/profiles/services/

    methods: GET

  - **show service details**

    path: */api/v1/profiles/services/<service_id>/

    methods: GET

  - **subscribe to service**

    path: */api/v1/profiles/services/<service_id>/subscribe/

    alternative path: */api/v1/profiles/users/<user_id>/subscriptions/create/<service_id>/

    methods: POST

## Build Setup


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# django

## Devel

Django signals:

- Reset password:
  allauth.account.signals.password_reset(request, user)


## Docs

https://django-allauth.readthedocs.io/en/latest/index.html

https://github.com/Tivix/django-rest-auth


## Test

Install Selenium and [chrome webdriver](http://chromedriver.chromium.org/downloads)

``` bash
# selenium
pip install selenium

# webdriver
chmod +x /opt/chromewebdriver/chromedriver
export PATH=$PATH:/opt/chromewebdriver/
```


## build frontend

``` bash
cd apps/frontend

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```


## Run tests
```
python manage.py test

```

