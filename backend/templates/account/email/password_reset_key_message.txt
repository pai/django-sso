{% load i18n %}

{% blocktrans %}
Hello from {{ EMAILS_SITE_NAME }}!

You're receiving this e-mail because you or someone else has requested a password for your user account.
It can be safely ignored if you did not request a password reset. Click the link below to reset your password.
{% endblocktrans %}


{% comment %}
rest_auth or allauth forms
{% endcomment %}
{% if password_reset_url %}{{ password_reset_url }}{% else %}{{ protocol }}://{{ EMAILS_DOMAIN }}{% url 'restauth_password_reset_confirm' uidb64=uid token=token %}{% endif %}

{% if username %}
{% blocktrans %}In case you forgot, your username is {{ username }}.{% endblocktrans %}
{% endif %}
{% blocktrans %}Thank you for using {{ EMAILS_SITE_NAME }}!{% endblocktrans %}
