import os
import sys
import raven


def env_to_bool(name, default=False):
    var = os.environ.get(name, '')
    res = default
    if default == True:
        if var in ['false', 'False', '0', 0]:
            res = False
    else:
        if var in ['true', 'True', '1', 1]:
            res = True
    return res

gettext = lambda s: s

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY', '1!d6@09rdfhm%gpofv=&e!&3bcivz%_dul9s!d9259)cz%=re1')

DEBUG = env_to_bool('DJANGO_DEBUG', True)


APP_DOMAIN = os.environ.get('APP_DOMAIN', 'localhost:8000')
_APP_DOMAINS = os.environ.get('APP_DOMAINS', APP_DOMAIN)
APP_DOMAINS = _APP_DOMAINS.split(',')

ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS', '*').split(',')

SSO_FRONTEND_APP = os.environ.get('SSO_FRONTEND_APP', 'apps.frontend')
NO_CUSTOM_SSO_FRONTEND_APP = SSO_FRONTEND_APP == 'apps.frontend'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # mine
    'django.contrib.sites',
    # 'django.contrib.messages',
    'django.contrib.staticfiles',

    # mine
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',

    'rest_auth.registration',
    'rest_framework_swagger',

    'corsheaders',

    'meta',

    'raven.contrib.django.raven_compat',

    'apps.profiles',
    'apps.services',
    'apps.devices',
    'apps.passepartout',

    'apps.api_gateway.kong',
    
    SSO_FRONTEND_APP,
]


MIDDLEWARE = [
    'backend.middleware.x_forwarded_for.middleware.XForwardedForMiddleware',

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    'corsheaders.middleware.CorsMiddleware',

    'django.middleware.locale.LocaleMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "backend", "templates"),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'backend.context_processors.google_api_settings',
                'backend.context_processors.google_analytics_settings',
                'backend.context_processors.raven_js_dsn_settings',

                'backend.context_processors.disable_js',
                'backend.context_processors.get_repository_rev',

                'backend.context_processors.django_meta'
            ]
        },
    },
]

WSGI_APPLICATION = 'backend.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ.get('PG_DB', 'accounts'),
            'USER': os.environ.get('PG_USER', 'accounts'),
            'PASSWORD': os.environ.get('PG_PASSWORD', 'accounts'),
            'HOST': os.environ.get('PG_HOST', 'localhost'),
            'PORT': '',
        }
    }

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'profiles.User'

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

# mine
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "backend", "static"),
]


SITE_ID = int(os.environ.get('DJANGO_SITE_ID', 1))

LANGUAGES = (
    ## Customize this
    ('it', gettext('it')),
    ('en', gettext('en')),
    ('es', gettext('es')),
    ('pt', gettext('pt')),
    ('fr', gettext('fr')),
    ('de', gettext('de')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

# allauth
AUTHENTICATION_BACKENDS = (

    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',

)
ACCOUNT_AUTHENTICATION_METHOD='username_email'
ACCOUNT_EMAIL_REQUIRED=True
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_ADAPTER='backend.adapter.UserAdapter'
ACCOUNT_CONFIRM_EMAIL_ON_GET=True
ACCOUNT_EMAIL_VERIFICATION='mandatory'
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS=False
#ACCOUNT_SESSION_REMEMBER=False
#AUTHENTICATED_LOGIN_REDIRECTS=True
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = LOGIN_URL
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = LOGIN_URL
ACCOUNT_SESSION_REMEMBER=False

ACCOUNT_FORMS = {
    'signup': 'backend.forms.SignupForm'
}


ENABLE_HTTPS = env_to_bool('ENABLE_HTTPS', False)

ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'http'
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False

if ENABLE_HTTPS:
    ACCOUNT_DEFAULT_HTTP_PROTOCOL='https'
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True

# email
DEFAULT_FROM_EMAIL = os.environ.get('DJANGO_DEFAULT_FROM_EMAIL', 'noreply@example.com')

# sendgrid
EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
SENDGRID_API_KEY=os.environ.get("SENDGRID_API_KEY", "")
#SENDGRID_SANDBOX_MODE_IN_DEBUG=False # defaults to True

if DEBUG or env_to_bool('DJANGO_CONSOLE_EMAIL_BACKEND'):
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


if DEBUG:
    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.TokenAuthentication',
            #'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
            'apps.profiles.authentication.ProfilesAuthentication',
            'rest_framework.authentication.SessionAuthentication',
        )
    }
else:
    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.TokenAuthentication',
            'apps.profiles.authentication.ProfilesAuthentication',
        )
    }

SWAGGER_SETTINGS = {
    'LOGIN_URL': '/login/',
    'LOGOUT_URL': '/logout/',
}

REST_USE_JWT = True
REST_AUTH_SERIALIZERS = {
    'LOGIN_SERIALIZER': 'backend.serializers.LoginSerializer',
    'USER_DETAILS_SERIALIZER' : 'apps.profiles.serializers.UserSerializer',
    'PASSWORD_RESET_SERIALIZER': 'backend.serializers.PasswordResetSerializer'
}

REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'backend.serializers.RegisterSerializer',
}

JWT_AUTH = {
    'JWT_AUTH_COOKIE': 'jwt',

    'JWT_PAYLOAD_HANDLER': 'backend.handlers.jwt_payload_handler',
    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
    'JWT_VERIFY': False,
}


SECURE_SSL_REDIRECT = env_to_bool('DJANGO_SECURE_SSL_REDIRECT')

COOKIE_DOMAIN = os.environ.get('COOKIE_DOMAIN', APP_DOMAIN)
APIGW_HOST = os.environ.get('APIGW_HOST', 'http://kong:8001')

APP_URL = ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + APP_DOMAIN

CONSUMER_GROUP = os.environ.get('CONSUMER_GROUP', COOKIE_DOMAIN) # apigw consumer ACL group name
CONSUMER_DOMAIN = os.environ.get('CONSUMER_DOMAIN', None) # if set will be attached to apigw consumer username

EMAILS_DOMAIN = os.environ.get('EMAILS_DOMAIN', APP_DOMAIN) # domain name specified in email templates
EMAILS_SITE_NAME = os.environ.get('EMAILS_SITE_NAME', EMAILS_DOMAIN) # site name specified in email templates

_SSO_DOMAINS = os.environ.get('SSO_DOMAINS', APP_DOMAIN)
SSO_DOMAINS = _SSO_DOMAINS.split(',')
SSO_DOMAINS_DICT = {}

i=0
for el in SSO_DOMAINS:
    SSO_DOMAINS_DICT[el] = i
    i += 1

SSO_FULL_URLS_CHAIN = [ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + el for el in SSO_DOMAINS]
SSO_URLS_CHAIN = [ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + el for el in SSO_DOMAINS if el != APP_DOMAIN]
SSO_URLS_CHAIN_DICT = {}
i = 0
for el in SSO_URLS_CHAIN:
    SSO_URLS_CHAIN_DICT[el] = i
    i += 1

PASSEPARTOUT_PROCESS_ENABLED = len(SSO_URLS_CHAIN) > 0

SSO_DEFAULT_REFERRER = os.environ.get('SSO_DEFAULT_REFERRER', ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + APP_DOMAIN)

SSO_HIDE_PASSWORD_FROM_USER_SERIALIZER = env_to_bool('SSO_HIDE_PASSWORD_FROM_USER_SERIALIZER', True)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
    },
    'loggers': {
        'django': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'profiles': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'devices': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'services': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'passepartout': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'api_gateway': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'backend': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

# https://github.com/ottoyiu/django-cors-headers
# CORS headers defaults to 'example.com,accounts.example.com'
_CORS_ORIGINS = os.environ.get('CORS_ORIGINS', '{0}://{1},{0}://{2}'.format(ACCOUNT_DEFAULT_HTTP_PROTOCOL, COOKIE_DOMAIN, 'accounts.{0}'.format(COOKIE_DOMAIN)))
CORS_ORIGIN_WHITELIST = list(map(lambda x: '{}'.format(x.replace(' ', '')), _CORS_ORIGINS.split(',')))
CORS_ALLOW_CREDENTIALS = True

# https://docs.djangoproject.com/en/2.0/ref/settings/#csrf-trusted-origins
CSRF_COOKIE_DOMAIN = APP_DOMAIN
CSRF_TRUSTED_ORIGINS = ['.{0}'.format(APP_DOMAIN)]

if DEBUG:
    CSRF_COOKIE_DOMAIN = None
    CORS_ORIGIN_ALLOW_ALL = True
    CSRF_TRUSTED_ORIGINS = []

TESTING_MODE = 'test' in sys.argv or DEBUG


RAVEN_DSN = os.environ.get('RAVEN_DSN', '')
RAVEN_JS_DSN = os.environ.get('RAVEN_JS_DSN', '')

if not DEBUG:
    RAVEN_CONFIG = {
        'dsn': RAVEN_DSN,
        # If you are using git, you can also automatically configure the
        # release based on the git info.
        'release': raven.fetch_git_sha(os.path.abspath(os.getcwd())),
    }

# Google API
GOOGLE_API_KEY = os.environ.get("GOOGLE_API_KEY", "AIzaSyBTWOm6kvTJC1SfKp41LjXYqyiiP0iKpJY")
GOOGLE_MAPS_API_VERSION = os.environ.get("GOOGLE_MAPS_API_VERSION", "3.34")

# Google Analytics
GOOGLE_ANALYTICS_TRACKING_ID = os.environ.get("GOOGLE_ANALYTICS_TRACKING_ID", "")

REPOSITORY_REV = os.environ.get("REPOSITORY_REV", None)

META_SITE_PROTOCOL = ACCOUNT_DEFAULT_HTTP_PROTOCOL
#META_SITE_DOMAIN = APP_DOMAIN
META_USE_SITES = True
META_SITE_NAME = EMAILS_SITE_NAME
META_DEFAULT_KEYWORDS = os.environ.get('META_DEFAULT_KEYWORDS', 'example,website').split(',')
META_DEFAULT_DESCRIPTION =  os.environ.get('META_DEFAULT_DESCRIPTION', APP_DOMAIN)

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',

    'django.contrib.auth.hashers.SHA1PasswordHasher'
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

CDN_ENABLED = env_to_bool('CDN_ENABLED')
CDN_DOMAIN = os.environ.get('CDN_DOMAIN', None)

if CDN_ENABLED and CDN_DOMAIN is not None:
    MEDIA_URL = ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + CDN_DOMAIN + '/media/'
    STATIC_URL = ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://' + CDN_DOMAIN + '/static/'

