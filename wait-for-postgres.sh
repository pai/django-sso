#!/bin/bash
# wait-for-postgres.sh

set -e

host="$1"
pass="$2"
shift
shift
cmd="$@"

export PGPASSWORD="$pass";

until ping -c 1 "$host" &>/dev/null; do
  >&2 echo "Postgres host is unavaiable - sleeping"
  sleep 1
done

until psql -h "$host" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing migrations"

exec $cmd

