��    2      �  C   <      H  �   I     E  	   M     W     e     m     y  &   �     �     �     �  
   �     �      �  �     
   �     �  2        9  	   B     L  
   U  	   `     j     p  	   w     �     �     �  "   �     �     �     �     �  )     $   0  O   U     �  �   �  �   J	     "
  	   *
     4
     <
  
   H
  	   S
     ]
  	   f
     p
  k  x
    �  	                  ,     2     >  *   E     p     �     �     �  
   �  "   �  �   �     �     �  B   �            
        )     ?     M     U     \     h     q     �     �      �     �     �     �  0   �       C   <  	   �  �   �  �   &  	   #     -     =     C     O     T  
   \     g     s        -                       
      0                    +       %                  /   !   1            '   $                ,      #                        "      .            )          &   (                        	   2           *    
Hello from %(EMAILS_SITE_NAME)s!

You're receiving this e-mail because you or someone else has requested a password for your user account.
It can be safely ignored if you did not request a password reset. Click the link below to reset your password.
 Address Birthdate Change E-mail Country Description Duration E-mail address not verified: %(value)s E-mail is not verified. Email address Email already registered. First name GOT IT! Hello from %(EMAILS_SITE_NAME)s! Hello from %(EMAILS_SITE_NAME)s!

You're receiving this e-mail because user %(user_display)s has given yours as an e-mail address to connect their account.

To confirm this is correct, go to %(activate_url)s
 IP address IPv4 address In case you forgot, your username is %(username)s. Language Last name Latitude Learn more Loading.. Login Logout Longitude Messages Organization Picture Please Confirm Your E-mail Address Please enable javascript. Register Sign In Successfully logged out. Thank you for using %(EMAILS_SITE_NAME)s! Thank you from %(EMAILS_SITE_NAME)s! This website uses cookies to ensure you get the best experience on our website. Welcome You're receiving this e-mail because user %(user_display)s has given yours as an e-mail address to connect their account.
To confirm this is correct, go to  You're receiving this e-mail because you or someone else has requested a password for your user account.
It can be safely ignored if you did not request a password reset. Click the link below to reset your password. address birthdate country description first name last name latitude longitude picture Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-22 21:49+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Saluti da %(EMAILS_SITE_NAME)s!

Hai ricevuto questa e-mail perchè tu o qualcun altro ha chiesto di reimpostare la password per il tuo account utente.
Puoi semplicemente ignorare questo messaggio se non sei stato tu a richiederlo. Clicca il link qui sotto per reimpostare la password.
 Indirizzo Data di nascita Cambia E-mail Paese Descrizione Durata Indirizzo E-mail non verificato: %(value)s E-mail non verificata. indirizzo Email Email già registrata. Nome HO CAPITO! Un saluto da %(EMAILS_SITE_NAME)s! Saluti da %(EMAILS_SITE_NAME)s!

Stai ricevendo questa e-mail perchè l'utente %(user_display)s ha associato questo indirizzo e-mail al suo account.

Per confermare che è corretto, vai su %(activate_url)s
 indirizzo IP indirizzo IPv4 Nel caso l'avessi dimenticato, il tuo nome utente è %(username)s. Lingua Cognome Latitudine Maggiori informazioni Caricamento.. Accesso Uscita Longitudine Messaggi Organizzazione Immagine Conferma indirizzo E-mail Per favore abilitare javascript. Registrazione Accesso Ti sei sloggato. Grazie per aver utilizzato %(EMAILS_SITE_NAME)s! Grazie da %(EMAILS_SITE_NAME)s! Questo sito usa i cookies per assicurarti una navigazione ottimale. Benvenuto Stai ricevendo questa e-mail perchè l'utente %(user_display)s ha associato questo indirizzo e-mail al suo account.
Per confermare che è corretto, vai su  Hai ricevuto questa e-mail perchè tu o qualcun altro ha chiesto di reimpostare la password per il tuo account utente.
Puoi semplicemente ignorare questo messaggio se non sei stato tu a richiederlo. Clicca il link qui sotto per reimpostare la password. indirizzo data di nascita paese descrizione nome cognome latitudine longitudine immagine 