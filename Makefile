DOCKER=docker
DPATH=${CURDIR}

export DPATH
.PHONY:	build push

build:
	$(DOCKER) build . -t paiuolo/django-sso -t paiuolo/django-sso:0.1.16

push:
	$(DOCKER) push paiuolo/django-sso:0.1.16
	$(DOCKER) push paiuolo/django-sso:latest

