#!/bin/bash

set -e

mkdir -p backup

python manage.py dumpdata --indent=3 --natural-primary --natural-foreign -e auth.permission -e contenttypes -e sessions -e admin.logentry -e devices -e passepartout > backup/dump.json
bzip2 backup/dump.json

# tar -zcvf backup/images.tar.gz ./media/filer_public
