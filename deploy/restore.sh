#!/bin/bash
set -e 

ls -l backup/

#tar -xvf backup/images.tar.gz -C .

python3 manage.py sqlflush | python3 manage.py dbshell

python3 manage.py migrate

bzip2 -d backup/dump.json.bz2
python3 manage.py loaddata backup/dump.json

#python3 manage.py thumbnail_cleanup

