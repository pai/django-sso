FROM paiuolo/python:3.5-jessie

MAINTAINER paiuolo@gmail.com


# config files
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
# COPY deploy/nginx.conf /etc/nginx/sites-available/default
COPY deploy/supervisord.docker.conf /etc/supervisor/conf.d/supervisord.docker.conf

WORKDIR /opt/django-sso

# create user
#RUN groupadd -r webapp -g 1000 && useradd -u 1000 -r -g webapp -m -d /opt/django-sso -s /sbin/nologin -c "Webapp user" user && \
#    chmod 755 /opt/django-sso


ENV APP_DOMAIN "example.com"
ENV APP_DOMAINS "example.com, www.example.com"
ENV COOKIE_DOMAIN "example.com"
ENV CORS_ORIGINS "example.com,www.example.com"
ENV CACHE_ENABLED "false"
ENV REDIS_HOST "redis"
ENV EMAIL_ENABLED "false"
ENV NPM_CONFIG_PREFIX=/opt/djangocms-pai/.npm-global


COPY requirements.txt .
RUN pip install -U pip
RUN pip install --no-cache-dir -r requirements.txt

COPY ./requirements/* ./requirements/
RUN find ./requirements/ -name '*.tar.gz' -exec pip install {} \;

RUN pip install --no-cache-dir --ignore-installed six

# copy
ADD . /opt/django-sso


EXPOSE 80

ENTRYPOINT ["./docker-entrypoint.sh"]
