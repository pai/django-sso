#!/bin/sh
set -e

sed -e "s/\${APP_DOMAIN}/$APP_DOMAIN/" deploy/nginx.conf.template > /etc/nginx/sites-enabled/default
sed -e "s/\${APP_DOMAINS}/$APP_DOMAINS/" deploy/nginx.conf.template > /etc/nginx/sites-enabled/default


python manage.py migrate
python manage.py loaddata fixtures/*.json

python manage.py collectstatic --noinput

chmod -R 664 /opt/django-sso/media
chmod -R a+X /opt/django-sso/media

exec supervisord -c /etc/supervisor/supervisord.conf
